use crate::async_helper::suspend;

use self::{
    instruction::Instruction,
    memory::Memory,
    registers::{Flags, Registers},
};

mod instruction;
mod memory;
mod registers;

#[derive(Debug)]
pub struct Cpu {
    registers: Registers,
    memory: Memory,
    stop: bool,
}

impl Cpu {
    pub fn new_with_rom(rom: &[u8]) -> Self {
        Cpu {
            registers: Registers {
                accumulator: 0,
                program_counter: 0,
                index_x: 0,
                index_y: 0,
                status: Flags::empty(),
            },
            memory: Memory::from_rom(rom),
            stop: false,
        }
    }

    pub async fn interpret(&mut self) {
        let low_byte = self.memory[0xFFFC];
        let high_byte = self.memory[0xFFFD];
        self.registers.program_counter = u16::from_le_bytes([low_byte, high_byte]);

        while !self.stop {
            let opcode = self.memory[self.registers.program_counter];
            self.registers.program_counter += 1;
            let instruction = Instruction::from(opcode);

            suspend().await;

            instruction.execute(self).await;
        }
    }

    pub async fn branch(&mut self, offset: u8) {
        let old_pc = self.registers.program_counter;
        self.registers.program_counter =
            (self.registers.program_counter as i16 + (offset as i8) as i16) as u16;
        suspend().await;
        if self.registers.program_counter & 0xFF00 != old_pc & 0xFF00 {
            suspend().await;
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        future::Future,
        pin::pin,
        sync::Arc,
        task::{Context, Wake, Waker},
    };

    use super::Cpu;

    pub fn test_cpu(mut rom: Vec<u8>) -> Cpu {
        let extension = vec![0; 0xFFFF - 0x8000 - rom.len()];
        rom.extend_from_slice(&extension);
        rom[0xFFFD - 0x8000] = 0x80;
        Cpu::new_with_rom(&rom)
    }

    pub fn run_test(cpu: &mut Cpu) {
        let waker = Waker::from(Arc::new(DummyWaker));
        let mut context = Context::from_waker(&waker);
        let mut future = pin!(cpu.interpret());
        loop {
            if future.as_mut().poll(&mut context).is_ready() {
                break;
            }
        }
    }

    struct DummyWaker;

    impl Wake for DummyWaker {
        fn wake(self: std::sync::Arc<Self>) {}
    }
}
