use std::ops::{Index, IndexMut};

#[derive(Debug)]
pub struct Memory([u8; 0xFFFF]);

impl Default for Memory {
    fn default() -> Self {
        Memory([0; 0xFFFF])
    }
}

impl Memory {
    pub fn load_rom(&mut self, program: &[u8]) {
        self.0[0x8000..0x8000 + program.len()].copy_from_slice(program);
    }

    pub fn from_rom(program: &[u8]) -> Self {
        let mut memory = Memory::default();
        memory.load_rom(program);
        memory
    }
}

impl Index<u16> for Memory {
    type Output = u8;

    fn index(&self, index: u16) -> &Self::Output {
        &self.0[index as usize]
    }
}

impl IndexMut<u16> for Memory {
    fn index_mut(&mut self, index: u16) -> &mut Self::Output {
        &mut self.0[index as usize]
    }
}
