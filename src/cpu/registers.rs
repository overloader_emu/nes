use bitflags::bitflags;

#[derive(Debug)]
pub struct Registers {
    /// A
    pub accumulator: u8,
    /// pc
    pub program_counter: u16,
    /// x
    pub index_x: u8,
    /// y
    pub index_y: u8,
    pub status: Flags,
}

impl Registers {
    pub fn update_zn_flags(&mut self, value: u8) {
        self.status.set(Flags::ZERO, value == 0);
        self.status
            .set(Flags::NEGATIVE, (value & 0b1000_0000) == 0b1000_0000);
    }
}

bitflags! {
    #[derive(Debug, PartialEq)]
    pub struct Flags: u8 {
        /// N
        const NEGATIVE = 0b1000_0000;
        /// V
        const OVERFLOW = 0b0100_0000;
        /// Z
        const ZERO = 0b0000_0010;
        /// C
        const CARRY = 0b0000_0001;
    }
}
