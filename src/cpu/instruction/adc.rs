use crate::cpu::{registers::Flags, Cpu};

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    let parameter = addressing.get_value(cpu).await;
    let (mut result, mut overflow) = cpu.registers.accumulator.overflowing_add(parameter);
    let old_carry = cpu.registers.status.contains(Flags::CARRY);
    cpu.registers.status.set(Flags::CARRY, overflow);
    if old_carry {
        (result, overflow) = result.overflowing_add(1);
        if !cpu.registers.status.contains(Flags::CARRY) {
            cpu.registers.status.set(Flags::CARRY, overflow);
        }
    }
    let accumulator_old = cpu.registers.accumulator;
    cpu.registers.accumulator = result;

    cpu.registers.status.set(
        Flags::OVERFLOW,
        ((accumulator_old ^ parameter) & 0x80) == 0
            && ((accumulator_old ^ cpu.registers.accumulator) & 0x80) != 0,
    );
    cpu.registers.update_zn_flags(cpu.registers.accumulator);
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn adc() {
        let mut cpu = test_cpu(vec![0x69, 1, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn adc_carry() {
        let mut cpu = test_cpu(vec![0x69, 0b1100_0000, 0x00]);
        cpu.registers.accumulator = 0b1100_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b1000_0000);
        assert_eq!(cpu.registers.status, Flags::CARRY | Flags::NEGATIVE);
    }

    #[test]
    fn adc_overflow() {
        let mut cpu = test_cpu(vec![0x69, 0b1000_0000, 0x00]);
        cpu.registers.accumulator = 0b1000_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b0000_0000);
        assert_eq!(
            cpu.registers.status,
            Flags::CARRY | Flags::OVERFLOW | Flags::ZERO
        );
    }
}
