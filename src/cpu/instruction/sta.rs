use crate::cpu::Cpu;

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    addressing.store_value(cpu, cpu.registers.accumulator).await;
}

#[cfg(test)]
mod tests {
    use crate::cpu::tests::{run_test, test_cpu};

    #[test]
    fn sta_zero_page() {
        let mut cpu = test_cpu(vec![0x85, 0, 0x00]);
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[0]);
    }

    #[test]
    fn sta_zero_page_indexed() {
        let mut cpu = test_cpu(vec![0x95, 0, 0x00]);
        cpu.registers.index_x = 1;
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[1]);
    }

    #[test]
    fn sta_absolute() {
        let mut cpu = test_cpu(vec![0x8D, 0, 0, 0x00]);
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[0]);
    }

    #[test]
    fn sta_absolute_indexed_x() {
        let mut cpu = test_cpu(vec![0x9D, 0, 0, 0x00]);
        cpu.registers.index_x = 1;
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[1]);
    }

    #[test]
    fn sta_absolute_indexed_y() {
        let mut cpu = test_cpu(vec![0x99, 0, 0, 0x00]);
        cpu.registers.index_y = 1;
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[1]);
    }

    #[test]
    fn sta_indirect_x() {
        let mut cpu = test_cpu(vec![0x81, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.registers.index_x = 1;
        cpu.memory[1] = 0x00;
        cpu.memory[2] = 0x00;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[0]);
    }

    #[test]
    fn sta_indirect_y() {
        let mut cpu = test_cpu(vec![0x91, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.registers.index_y = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, cpu.memory[1]);
    }
}
