use crate::cpu::{Cpu, registers::Flags};

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    let parameter = addressing.get_value(cpu).await;
    let masked = parameter & cpu.registers.accumulator;
    
    cpu.registers.status.set(Flags::ZERO, masked == 0);
    cpu.registers.status.set(Flags::OVERFLOW, parameter & 0b0100_0000 == 0b0100_0000);
    cpu.registers.status.set(Flags::NEGATIVE, parameter & 0b1000_0000 == 0b1000_0000);
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn bit_zero() {
        let mut cpu = test_cpu(vec![0x2C, 4, 0x80, 0x00, 1]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.status, Flags::ZERO);

        let mut cpu = test_cpu(vec![0x2C, 4, 0x80, 0x00, 1]);
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.status, Flags::empty());
    }
}
