use self::addressing::{Addressing, Register};

use super::Cpu;

mod adc;
mod addressing;
mod and;
mod asl;
mod bcc;
mod bcs;
mod beq;
mod bit;
mod bmi;
mod bne;
mod brk;
mod inx;
mod lda;
mod sta;
mod tax;

pub enum Instruction {
    /// ADC
    AddWithCarry(Addressing),
    /// AND
    LogicalAnd(Addressing),
    /// ASL
    ArithmeticShiftLeft(Addressing),
    /// BCC
    BranchCarryClear,
    /// BCS
    BranchCarrySet,
    /// BEQ
    BranchEqual,
    /// BIT
    BitTest(Addressing),
    /// BMI
    BranchMinus,
    /// BNE
    BranchNotEqual,
    /// BRK
    ForceInterrupt,
    /// INX
    IncrementX,
    /// LDA
    LoadAccumulator(Addressing),
    /// STA
    StoreAccumulator(Addressing),
    /// TAX
    TransferAccumulatorToX,
}

impl From<u8> for Instruction {
    fn from(value: u8) -> Self {
        match value {
            // ADC
            0x69 => Instruction::AddWithCarry(Addressing::Immediate),
            0x65 => Instruction::AddWithCarry(Addressing::ZeroPage),
            0x75 => Instruction::AddWithCarry(Addressing::ZeroPageIndexed(Register::X)),
            0x6D => Instruction::AddWithCarry(Addressing::Absolute),
            0x7D => Instruction::AddWithCarry(Addressing::AbsoluteIndexed(Register::X)),
            0x79 => Instruction::AddWithCarry(Addressing::AbsoluteIndexed(Register::Y)),
            0x61 => Instruction::AddWithCarry(Addressing::Indirect(Register::X)),
            0x71 => Instruction::AddWithCarry(Addressing::Indirect(Register::Y)),

            // AND
            0x29 => Instruction::LogicalAnd(Addressing::Immediate),
            0x25 => Instruction::LogicalAnd(Addressing::ZeroPage),
            0x35 => Instruction::LogicalAnd(Addressing::ZeroPageIndexed(Register::X)),
            0x2D => Instruction::LogicalAnd(Addressing::Absolute),
            0x3D => Instruction::LogicalAnd(Addressing::AbsoluteIndexed(Register::X)),
            0x39 => Instruction::LogicalAnd(Addressing::AbsoluteIndexed(Register::Y)),
            0x21 => Instruction::LogicalAnd(Addressing::Indirect(Register::X)),
            0x31 => Instruction::LogicalAnd(Addressing::Indirect(Register::Y)),

            // ASL
            0x0A => Instruction::ArithmeticShiftLeft(Addressing::Accumulator),
            0x06 => Instruction::ArithmeticShiftLeft(Addressing::ZeroPage),
            0x16 => Instruction::ArithmeticShiftLeft(Addressing::ZeroPageIndexed(Register::X)),
            0x0E => Instruction::ArithmeticShiftLeft(Addressing::Absolute),
            0x1E => Instruction::ArithmeticShiftLeft(Addressing::AbsoluteIndexed(Register::X)),

            // BCC
            0x90 => Instruction::BranchCarryClear,

            // BCC
            0xB0 => Instruction::BranchCarrySet,

            // BEQ
            0xF0 => Instruction::BranchEqual,

            // BIT
            0x24 => Instruction::BitTest(Addressing::ZeroPage),
            0x2C => Instruction::BitTest(Addressing::Absolute),

            // BMI
            0x30 => Instruction::BranchMinus,

            // BNE
            0xD0 => Instruction::BranchNotEqual,

            // BRK
            0x00 => Instruction::ForceInterrupt,

            // INX
            0xE8 => Instruction::IncrementX,

            // LDA
            0xA9 => Instruction::LoadAccumulator(Addressing::Immediate),
            0xA5 => Instruction::LoadAccumulator(Addressing::ZeroPage),
            0xB5 => Instruction::LoadAccumulator(Addressing::ZeroPageIndexed(Register::X)),
            0xAD => Instruction::LoadAccumulator(Addressing::Absolute),
            0xBD => Instruction::LoadAccumulator(Addressing::AbsoluteIndexed(Register::X)),
            0xB9 => Instruction::LoadAccumulator(Addressing::AbsoluteIndexed(Register::Y)),
            0xA1 => Instruction::LoadAccumulator(Addressing::Indirect(Register::X)),
            0xB1 => Instruction::LoadAccumulator(Addressing::Indirect(Register::Y)),

            // STA
            0x85 => Instruction::StoreAccumulator(Addressing::ZeroPage),
            0x95 => Instruction::StoreAccumulator(Addressing::ZeroPageIndexed(Register::X)),
            0x8D => Instruction::StoreAccumulator(Addressing::Absolute),
            0x9D => Instruction::StoreAccumulator(Addressing::AbsoluteIndexed(Register::X)),
            0x99 => Instruction::StoreAccumulator(Addressing::AbsoluteIndexed(Register::Y)),
            0x81 => Instruction::StoreAccumulator(Addressing::Indirect(Register::X)),
            0x91 => Instruction::StoreAccumulator(Addressing::Indirect(Register::Y)),

            // TAX
            0xAA => Instruction::TransferAccumulatorToX,

            _ => panic!("{value:#0x} not yet implemented"),
        }
    }
}

impl Instruction {
    pub async fn execute(&self, cpu: &mut Cpu) {
        match self {
            Instruction::AddWithCarry(addressing) => adc::execute(addressing, cpu).await,
            Instruction::LogicalAnd(addressing) => and::execute(addressing, cpu).await,
            Instruction::ArithmeticShiftLeft(addressing) => asl::execute(addressing, cpu).await,
            Instruction::BranchCarryClear => bcc::execute(cpu).await,
            Instruction::BranchCarrySet => bcs::execute(cpu).await,
            Instruction::BranchEqual => beq::execute(cpu).await,
            Instruction::BitTest(addressing) => bit::execute(addressing, cpu).await,
            Instruction::BranchMinus => bmi::execute(cpu).await,
            Instruction::BranchNotEqual => bne::execute(cpu).await,
            Instruction::ForceInterrupt => brk::execute(cpu),
            Instruction::IncrementX => inx::execute(cpu).await,
            Instruction::LoadAccumulator(addressing) => lda::execute(addressing, cpu).await,
            Instruction::StoreAccumulator(addressing) => sta::execute(addressing, cpu).await,
            Instruction::TransferAccumulatorToX => tax::execute(cpu).await,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::cpu::tests::{run_test, test_cpu};

    #[test]
    fn test_5_ops_working_together() {
        let mut cpu = test_cpu(vec![0xA9, 0xC0, 0xAA, 0xE8, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.index_x, 0xc1)
    }
}
