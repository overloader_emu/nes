use crate::cpu::Cpu;

pub fn execute(cpu: &mut Cpu) {
    cpu.stop = true;
}
