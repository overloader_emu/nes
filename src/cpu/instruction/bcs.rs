use crate::cpu::{Cpu, registers::Flags};

use super::addressing::Addressing;

pub async fn execute(cpu: &mut Cpu) {
    let parameter = Addressing::Immediate.get_value(cpu).await;
    if cpu.registers.status.contains(Flags::CARRY) {
        cpu.branch(parameter).await;
    }
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn bcs_set() {
        let mut cpu = test_cpu(vec![0xB0, 2, 0x00]);
        cpu.registers.status.set(Flags::CARRY, true);
        let pc_start = u16::from_le_bytes([cpu.memory[0xFFFC], cpu.memory[0xFFFD]]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.program_counter, 5 + pc_start);
    }

    #[test]
    fn bcc_clear() {
        let mut cpu = test_cpu(vec![0xB0, 2, 0x00]);
        let pc_start = u16::from_le_bytes([cpu.memory[0xFFFC], cpu.memory[0xFFFD]]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.program_counter, 3 + pc_start);
    }
}
