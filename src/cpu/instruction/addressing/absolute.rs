use crate::{async_helper::suspend, cpu::Cpu};

pub async fn get_value(cpu: &mut Cpu) -> u8 {
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let address = u16::from_le_bytes([low_byte, high_byte]);
    let value = cpu.memory[address];
    suspend().await;

    value
}

pub async fn store_value(cpu: &mut Cpu, value: u8) {
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let address = u16::from_le_bytes([low_byte, high_byte]);
    cpu.memory[address] = value;
    suspend().await;
}

pub async fn modify<F>(cpu: &mut Cpu, callback: F) -> u8
where
    F: FnOnce(u8, &mut Cpu) -> u8,
{
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter + 1];
    cpu.registers.program_counter += 1;
    suspend().await;

    let address = u16::from_be_bytes([low_byte, high_byte]);
    let data = cpu.memory[address as u16];
    suspend().await;

    let data = callback(data, cpu);
    suspend().await;

    cpu.memory[address as u16] = data;
    data
}
