use crate::cpu::Cpu;

pub async fn modify<F>(cpu: &mut Cpu, callback: F) -> u8
where
    F: FnOnce(u8, &mut Cpu) -> u8,
{
    cpu.registers.accumulator = callback(cpu.registers.accumulator, cpu);
    cpu.registers.accumulator
}
