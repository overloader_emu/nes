use crate::{async_helper::suspend, cpu::Cpu};

use super::Register;

pub async fn get_value(cpu: &mut Cpu, register: &Register) -> u8 {
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    let (low_byte, overflow) = low_byte.overflowing_add(index_value);
    let address = u16::from_le_bytes([low_byte, high_byte]);
    let value = if overflow {
        cpu.memory[address];
        suspend().await;

        let high_byte = high_byte + 1;
        let address = u16::from_le_bytes([low_byte, high_byte]);
        cpu.memory[address]
    } else {
        cpu.memory[address]
    };
    suspend().await;

    value
}

pub async fn store_value(cpu: &mut Cpu, register: &Register, value: u8) {
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    let address = u16::from_le_bytes([low_byte, high_byte]) + index_value as u16;
    suspend().await;

    cpu.memory[address] = value;
    suspend().await;
}

pub async fn modify<F>(cpu: &mut Cpu, register: &Register, callback: F) -> u8
where
    F: FnOnce(u8, &mut Cpu) -> u8,
{
    let low_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let high_byte = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let address = u16::from_le_bytes([low_byte, high_byte]);
    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    let address = address.wrapping_add(index_value as u16);
    suspend().await;

    let data = cpu.memory[address];
    suspend().await;

    let data = callback(data, cpu);
    suspend().await;

    cpu.memory[address] = data;
    data
}
