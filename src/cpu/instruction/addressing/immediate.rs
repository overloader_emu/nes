use crate::{async_helper::suspend, cpu::Cpu};

pub async fn get_value(cpu: &mut Cpu) -> u8 {
    let value = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;

    suspend().await;

    value
}
