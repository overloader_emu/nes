use crate::{async_helper::suspend, cpu::Cpu};

mod absolute;
mod absolute_indexed;
mod accumulator;
mod immediate;
mod indirect;
mod zero_page;
mod zero_page_indexed;

/// Some addressing mode can use both x and y registers
pub enum Register {
    X,
    Y,
}

pub enum Addressing {
    Absolute,
    AbsoluteIndexed(Register),
    Accumulator,
    Immediate,
    Indirect(Register),
    ZeroPage,
    ZeroPageIndexed(Register),
}

impl Addressing {
    pub async fn get_value(&self, cpu: &mut Cpu) -> u8 {
        suspend().await;
        match self {
            Addressing::Absolute => absolute::get_value(cpu).await,
            Addressing::AbsoluteIndexed(register) => {
                absolute_indexed::get_value(cpu, register).await
            }
            Addressing::Immediate => immediate::get_value(cpu).await,
            Addressing::Indirect(register) => indirect::get_value(cpu, register).await,
            Addressing::ZeroPage => zero_page::get_value(cpu).await,
            Addressing::ZeroPageIndexed(register) => {
                zero_page_indexed::get_value(cpu, register).await
            }
            _ => unreachable!(),
        }
    }

    pub async fn store_value(&self, cpu: &mut Cpu, value: u8) {
        suspend().await;
        match self {
            Addressing::Absolute => absolute::store_value(cpu, value).await,
            Addressing::AbsoluteIndexed(register) => {
                absolute_indexed::store_value(cpu, register, value).await
            }
            Addressing::Immediate => todo!(),
            Addressing::Indirect(register) => indirect::store_value(cpu, register, value).await,
            Addressing::ZeroPage => zero_page::store_value(cpu, value).await,
            Addressing::ZeroPageIndexed(register) => {
                zero_page_indexed::store_value(cpu, register, value).await
            }
            _ => unreachable!(),
        }
    }

    pub async fn modify<F>(&self, cpu: &mut Cpu, callback: F) -> u8
    where
        F: FnOnce(u8, &mut Cpu) -> u8,
    {
        suspend().await;
        match self {
            Addressing::Absolute => absolute::modify(cpu, callback).await,
            Addressing::AbsoluteIndexed(register) => {
                absolute_indexed::modify(cpu, register, callback).await
            }
            Addressing::Accumulator => accumulator::modify(cpu, callback).await,
            Addressing::ZeroPage => zero_page::modify(cpu, callback).await,
            Addressing::ZeroPageIndexed(register) => {
                zero_page_indexed::modify(cpu, register, callback).await
            }
            _ => unreachable!(),
        }
    }
}
