use crate::{async_helper::suspend, cpu::Cpu};

pub async fn get_value(cpu: &mut Cpu) -> u8 {
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;

    suspend().await;

    let value = cpu.memory[address as u16];

    suspend().await;

    value
}

pub async fn store_value(cpu: &mut Cpu, value: u8) {
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    cpu.memory[address as u16] = value;
    suspend().await;
}

pub async fn modify<F>(cpu: &mut Cpu, callback: F) -> u8
where
    F: FnOnce(u8, &mut Cpu) -> u8,
{
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    let data = cpu.memory[address as u16];
    suspend().await;

    let data = callback(data, cpu);
    suspend().await;

    cpu.memory[address as u16] = data;
    data
}
