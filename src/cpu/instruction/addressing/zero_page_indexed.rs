use crate::{async_helper::suspend, cpu::Cpu};

use super::Register;

pub async fn get_value(cpu: &mut Cpu, register: &Register) -> u8 {
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    // A wasted cycle
    cpu.memory[address as u16];
    suspend().await;

    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    let value = cpu.memory[(address.wrapping_add(index_value)) as u16];
    suspend().await;

    value
}

pub async fn store_value(cpu: &mut Cpu, register: &Register, value: u8) {
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    // A wasted cycle
    suspend().await;

    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    cpu.memory[(address.wrapping_add(index_value)) as u16] = value;
    suspend().await;
}

pub async fn modify<F>(cpu: &mut Cpu, register: &Register, callback: F) -> u8
where
    F: FnOnce(u8, &mut Cpu) -> u8,
{
    let address = cpu.memory[cpu.registers.program_counter];
    cpu.registers.program_counter += 1;
    suspend().await;

    cpu.memory[address as u16];
    suspend().await;

    let index_value = match register {
        Register::X => cpu.registers.index_x,
        Register::Y => cpu.registers.index_y,
    };
    let address = address.wrapping_add(index_value);
    let data = cpu.memory[address as u16];
    suspend().await;

    let data = callback(data, cpu);
    suspend().await;

    cpu.memory[address as u16] = data;
    data
}
