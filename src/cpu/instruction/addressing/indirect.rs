use crate::cpu::Cpu;

use super::Register;

pub async fn get_value(cpu: &mut Cpu, register: &Register) -> u8 {
    match register {
        Register::X => x::get_value(cpu).await,
        Register::Y => y::get_value(cpu).await,
    }
}

pub async fn store_value(cpu: &mut Cpu, register: &Register, value: u8) {
    match register {
        Register::X => x::store_value(cpu, value).await,
        Register::Y => y::store_value(cpu, value).await,
    }
}

mod x {
    use crate::{async_helper::suspend, cpu::Cpu};

    pub async fn get_value(cpu: &mut Cpu) -> u8 {
        let indirect_address = cpu.memory[cpu.registers.program_counter];
        cpu.registers.program_counter += 1;
        suspend().await;

        cpu.memory[indirect_address as u16];
        suspend().await;

        let low_byte = cpu.memory[indirect_address.wrapping_add(cpu.registers.index_x) as u16];
        suspend().await;

        let high_byte = cpu.memory[indirect_address
            .wrapping_add(cpu.registers.index_x)
            .wrapping_add(1) as u16];
        suspend().await;

        let address = u16::from_le_bytes([low_byte, high_byte]);
        let data = cpu.memory[address];
        suspend().await;

        data
    }

    pub async fn store_value(cpu: &mut Cpu, value: u8) {
        let indirect_address = cpu.memory[cpu.registers.program_counter];
        cpu.registers.program_counter += 1;
        suspend().await;

        cpu.memory[indirect_address as u16];
        suspend().await;

        let low_byte = cpu.memory[indirect_address.wrapping_add(cpu.registers.index_x) as u16];
        suspend().await;

        let high_byte = cpu.memory[indirect_address
            .wrapping_add(cpu.registers.index_x)
            .wrapping_add(1) as u16];
        suspend().await;

        let address = u16::from_le_bytes([low_byte, high_byte]);
        cpu.memory[address] = value;
        suspend().await;
    }
}

mod y {
    use crate::{async_helper::suspend, cpu::Cpu};

    pub async fn get_value(cpu: &mut Cpu) -> u8 {
        let indirect_address = cpu.memory[cpu.registers.program_counter];
        cpu.registers.program_counter += 1;
        suspend().await;

        let low_byte = cpu.memory[indirect_address as u16];
        suspend().await;

        let high_byte = cpu.memory[indirect_address as u16 + 1];
        suspend().await;

        let (low_byte, overflow) = low_byte.overflowing_add(cpu.registers.index_y);
        let value = {
            let mut address = u16::from_le_bytes([low_byte, high_byte]);
            let mut value = cpu.memory[address];
            if overflow {
                suspend().await;

                address = u16::from_le_bytes([low_byte, high_byte + 1]);
                value = cpu.memory[address];
            }
            value
        };
        suspend().await;

        value
    }

    pub async fn store_value(cpu: &mut Cpu, value: u8) {
        let indirect_address = cpu.memory[cpu.registers.program_counter];
        cpu.registers.program_counter += 1;
        suspend().await;

        let low_byte = cpu.memory[indirect_address as u16];
        suspend().await;

        let high_byte = cpu.memory[indirect_address as u16 + 1];
        suspend().await;

        let address = u16::from_le_bytes([low_byte, high_byte]) + cpu.registers.index_y as u16;
        suspend().await;

        cpu.memory[address] = value;
        suspend().await;
    }
}
