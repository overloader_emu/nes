use crate::{async_helper::suspend, cpu::Cpu};

pub async fn execute(cpu: &mut Cpu) {
    suspend().await;
    cpu.registers.index_x = cpu.registers.index_x.wrapping_add(1);
    cpu.registers.update_zn_flags(cpu.registers.index_x);
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn inx() {
        let mut cpu = test_cpu(vec![0xE8, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.index_x, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn inx_overflow() {
        let mut cpu = test_cpu(vec![0xE8, 0xE8, 0x00]);

        cpu.registers.index_x = 0xFF;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.index_x, 1)
    }
}
