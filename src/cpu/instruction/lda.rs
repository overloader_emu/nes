use crate::cpu::Cpu;

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    let parameter = addressing.get_value(cpu).await;
    cpu.registers.accumulator = parameter;
    cpu.registers.update_zn_flags(cpu.registers.accumulator);
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn lda_and_immediate_addressing() {
        let mut cpu = test_cpu(vec![0xa9, 1, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_zero_flag() {
        let mut cpu = test_cpu(vec![0xa9, 0, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0);
        assert_eq!(cpu.registers.status, Flags::ZERO);
    }

    #[test]
    fn lda_negative_flag() {
        let mut cpu = test_cpu(vec![0xa9, u8::MAX, 0x00]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, u8::MAX);
        assert_eq!(cpu.registers.status, Flags::NEGATIVE);
    }

    #[test]
    fn lda_zero_page() {
        let mut cpu = test_cpu(vec![0xA5, 0, 0x00]);
        cpu.memory[0] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_zero_page_indexed() {
        let mut cpu = test_cpu(vec![0xb5, 0, 0x00]);
        cpu.registers.index_x = 1;
        cpu.memory[1] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_absolute() {
        let mut cpu = test_cpu(vec![0xAD, 4, 0x80, 0x00, 1]);

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_absolute_indexed_x() {
        let mut cpu = test_cpu(vec![0xBD, 3, 0x80, 0x00, 1]);
        cpu.registers.index_x = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_absolute_indexed_y() {
        let mut cpu = test_cpu(vec![0xB9, 3, 0x80, 0x00, 1]);
        cpu.registers.index_y = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_indirect_x() {
        let mut cpu = test_cpu(vec![0xA1, 0, 0x00, 1]);
        cpu.registers.index_x = 1;
        cpu.memory[1] = 0x03;
        cpu.memory[2] = 0x80;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn lda_indirect_y() {
        let mut cpu = test_cpu(vec![0xB1, 0, 0x00, 1]);
        cpu.registers.index_y = 1;
        cpu.memory[0] = 0x02;
        cpu.memory[1] = 0x80;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 1);
        assert_eq!(cpu.registers.status, Flags::empty());
    }
}
