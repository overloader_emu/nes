use crate::cpu::Cpu;

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    let parameter = addressing.get_value(cpu).await;
    cpu.registers.accumulator &= parameter;

    cpu.registers.update_zn_flags(cpu.registers.accumulator);
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn and() {
        let mut cpu = test_cpu(vec![0x29, 0b0100_0001, 0x00]);
        cpu.registers.accumulator = 0b0000_1001;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b0000_0001);
        assert_eq!(cpu.registers.status, Flags::empty());
    }
}
