use crate::{
    async_helper::suspend,
    cpu::{registers::Flags, Cpu},
};

use super::addressing::Addressing;

pub async fn execute(addressing: &Addressing, cpu: &mut Cpu) {
    let result = addressing
        .modify(cpu, |value, cpu| {
            cpu.registers
                .status
                .set(Flags::CARRY, value & 0b1000_0000 == 0b1000_0000);
            value << 1
        })
        .await;
    cpu.registers
        .status
        .set(Flags::ZERO, cpu.registers.accumulator == 0);
    cpu.registers
        .status
        .set(Flags::NEGATIVE, result & 0b1000_0000 == 0b1000_0000);
    suspend().await;
}

#[cfg(test)]
mod tests {
    use crate::cpu::{
        registers::Flags,
        tests::{run_test, test_cpu},
    };

    #[test]
    fn asl_accumulator() {
        let mut cpu = test_cpu(vec![0x0A, 0x00]);
        cpu.registers.accumulator = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 2);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn asl_zero_page() {
        let mut cpu = test_cpu(vec![0x06, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.memory[0] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 2);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn asl_zero_page_indexed() {
        let mut cpu = test_cpu(vec![0x16, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.registers.index_x = 1;
        cpu.memory[1] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[1], 2);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn asl_absolute() {
        let mut cpu = test_cpu(vec![0x0E, 0, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.memory[0] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 2);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn asl_absolute_indexed() {
        let mut cpu = test_cpu(vec![0x1E, 0, 0, 0x00]);
        cpu.registers.accumulator = 1;
        cpu.registers.index_x = 1;
        cpu.memory[1] = 1;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[1], 2);
        assert_eq!(cpu.registers.status, Flags::empty());
    }

    #[test]
    fn asl_accumulator_flags() {
        let mut cpu = test_cpu(vec![0x0A, 0x00]);
        cpu.registers.accumulator = 0b1000_0001;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b0000_0010);
        assert_eq!(cpu.registers.status, Flags::CARRY);

        let mut cpu = test_cpu(vec![0x0A, 0x00]);
        cpu.registers.accumulator = 0b0000_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b0000_0000);
        assert_eq!(cpu.registers.status, Flags::ZERO);

        let mut cpu = test_cpu(vec![0x0A, 0x00]);
        cpu.registers.accumulator = 0b0100_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.registers.accumulator, 0b1000_0000);
        assert_eq!(cpu.registers.status, Flags::NEGATIVE);
    }

    #[test]
    fn asl_memory_flags() {
        let mut cpu = test_cpu(vec![0x06, 0x00, 0x00]);
        cpu.registers.accumulator = 0b0000_0001;
        cpu.memory[0] = 0b1000_0001;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 0b0000_0010);
        assert_eq!(cpu.registers.status, Flags::CARRY);

        let mut cpu = test_cpu(vec![0x06, 0x00, 0x00]);
        cpu.registers.accumulator = 0b0000_0000;
        cpu.memory[0] = 0b0000_0001;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 0b0000_0010);
        assert_eq!(cpu.registers.status, Flags::ZERO);

        let mut cpu = test_cpu(vec![0x06, 0x00, 0x00]);
        cpu.registers.accumulator = 0b0000_0001;
        cpu.memory[0] = 0b0000_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 0b0000_0000);
        assert_eq!(cpu.registers.status, Flags::empty());

        let mut cpu = test_cpu(vec![0x06, 0x00, 0x00]);
        cpu.registers.accumulator = 0b0000_0001;
        cpu.memory[0] = 0b0100_0000;

        run_test(&mut cpu);

        assert_eq!(cpu.memory[0], 0b1000_0000);
        assert_eq!(cpu.registers.status, Flags::NEGATIVE);
    }
}
