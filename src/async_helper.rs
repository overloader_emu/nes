use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

pub enum Suspend {
    Pending,
    Done,
}

impl Future for Suspend {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Self::Output> {
        match &*self {
            Suspend::Pending => {
                *self = Suspend::Done;
                Poll::Pending
            }
            Suspend::Done => Poll::Ready(()),
        }
    }
}

pub fn suspend() -> Suspend {
    Suspend::Pending
}
